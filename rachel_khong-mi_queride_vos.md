# Mi queride vos, de Rachel Khong

> Traducción de My dear you, en https://tinhouse.com/my-dear-you/

Seleccioné cincuenta y cuatro milímetros para el espacio entre mis ojos. Durante toda mi vida, mis ojos habían estado muy distanciados. Cuando niña, les otres me llamaban «cabeza de martillo». Algo que nadie te dice es que cuando morís de algo que deja tu cara y tu cuerpo completamente mutilados podés escoger tu cara en el cielo. Tu cuerpo, hasta cierto punto, también, porque te dan un cuerpo que corresponde con la cara que escogiste. Pero el color de piel, la forma de la nariz, los labios y los dientes, todo depende de vos. Ese es el lado bueno.

«Tomate tu tiempo», me dijo Richard. Su posición y rango era no se qué. Pensaba en él como mi carcelero, aunque esto estaba bastante segura que era el cielo. Él hablaba en frases cortas y autoritarias, y parecía aburrido, como los carceleros en la tele. Es solo lo que yo pensaba de él, no estoy diciendo que así fuera.

Estábamos vacacionando en Australia, Adam y yo. Fue un cocodrilo el que me asesinó, que sujetó mi cara y mi cuerpo con sus muchos y filosos dientes, y aplastó mis huesos. Pero no me masticó. Eso no lo hacen. Se tragan su presa entera y pulverizan su comida con incontables piedrecitas en sus tractos digestivos. Si su presa es muy grande para tragársela entera, primero la desgarran en piezas más pequeñas. El desgarramiento y las piedras dejaron mi cara totalmente jodida. Las «lágrimas de cocodrilo» son lágrimas insinceras porque los cocodrilos lloran cuando devoran a su presa. ¿Este lloró cuando me comió? Me dijeron que sí, porque eso es lo que hacen sus glándulas y ya. ¿Cuántos años tenía? Me dijeron que setenta, lo que significa que este cocodrilo vivió cuarenta años más que yo. No se si sentirme feliz o triste por esto. ¿Era macho o hembra? Me dijeron que hembra. Mi vida la terminó una perra cocodrila, aunque así no se dice, se dice cocodrilo hembra.

Le pregunté a Richard cuánto tiempo tenía realmente para meditar sobre mi cara y estos aspectos de mi cuerpo que me estaban permitiendo rehacer. Me dijo que veinticuatro horas, pero que las horas en el cielo funcionaban un poco diferente.

«Por qué, necesitás una extensión», dijo sin inflexión.

Le dije que estaba bien. Richard ha sido tan paciente y servicial con las preguntas que había hecho hasta ahora que en realidad no quería incomodarle más.

«¿Cómo funciona esto?», le pregunté.

«¿Qué parte?»

Quería saber para qué sería la cara. ¿Comería con mi nueva boca? ¿Me broncearía con mi nueva piel? Y otra pregunta que tuve, ¿había posibilidad de que me enamorara de nuevo?

Él dijo que sí a todo, y que pronto vería que la muerte no era tan diferente a la «vida» - él usó comillas. Solo que habría gente nueva. Gente nueva para mi. En ese momento vi a David Bowie pasar en bicicleta. Pero, ¿era David Bowie o alguien que escogió su cara? Richard no estaba seguro.

Escogí ojos verdes tirando a café, con manchitas. Había un programa de computadora para todo esto, y todo lo que yo tenía que hacer era poner características en una cesta como de carrito de compras, como si estuviera haciendo compras en línea, y se mostraba casi de forma instantánea en mi cara, que luego podía revisar en el espejo para ver si se ajustaba. Escogí mis estilo original de orejas, una forma de bombillo, con lóbulos suaves y una pelusa como de melocotón, porque a Adam le gustaban. En nuestro cuarto de hotel la noche anterior al accidente yo le había cuestionado qué le gustaba más de mi. Estábamos en nuestra luna de miel, ¿pueden creerlo? Lóbulos, muñeca, sonrisa, me dijo mientras me mostraba la suya cursi. Resulta que yo no tenía una opinión sobre las orejas. Todas me parecían funcionales. Entonces nada más escogí mis viejas orejas pero hice que se salieran menos. Nunca me gustaron mis dientes entonces también los cambié. Los hice un poco más grandes y más blancos y más rectangulares. Adam hubiera odiado esto, pero el no estaba aquí, ¿cierto? Teníamos 30 años de edad y justo nos habías prometido, bajo un kiosco, en frente de la mayoría de personas que nos importaban, que pasaríamos el resto de nuestras vidas juntes. Y así lo hicimos, yo cumplí mi promesa.

No estaba segura si quería seguir siendo una persona china, entonces dejé ese detalle para el final. Se sintió poco ético poder escoger. Cerré mis ojos e hice clic en un lugar aleatorio de la pantalla. China, dijo, y mis ojos se afilaron el los bordes, la forma de almendra que había tenido toda mi vida. Mentiría si dijera que no estuve un poco decaída.

Lo que Richard quería decir sobre el tiempo siendo raro es que une lo sentía diferente. Un día estaba jugando raquetbol con mi nueva amiga Heidi, y nos estábamos divirtiendo un montón, golpeando como maniáticas, sin saber cómo se jugaba porque nunca antes habíamos jugado. Luego escuchamos golpes enojados en la puerta. Era una pareja, sosteniendo sus raquetas y con apariencia malhumorada. «Podemos POR FAVOR usar el cuarto», dijo la mujer. «Hemos estado esperando». Tenía unas cejas gruesas que me levantó, y ningún hueso facial visible. Era claro que esta había sido su cara mientras vivía y que perdió su vida de forma pacífica, tal vez mientras dormía, o de cáncer. No quiero ofender, pero vos entendés lo que quiero decir. El hombre fue menos confrontativo. Él se me quedó viendo por un rato demasiado largo, a mis rasgos perfectos, o tal vez tenía un fetiche asiático. Luego se volvió a ver los pies. «Lo sentimos», dijo Heidi, evitando, con su acento sureño. Cuando vimos nuestros relojes nos dimos cuenta que habíamos estado ahí cuatro años.

Conforme pasaba el tiempo podía imaginar las caras de mi vida con menos claridad. La de mi madre fue la primera en irse; luego la de mi padre. Aunque había visto sus caras por treinta años. Aunque te diría que conocía sus caras de forma íntima, casi tan íntima como conocía mi propia cara, que también empecé a olvidar. Preocupantemente, la cara de mi jefe permaneció clara. Y su cuerpo. Presumido, despistado, pastoso. Les otres redactores y yo vivíamos en miedo constante. Él era lo que llamaríamos un «jefe tóxico». Una vez leí un artículo sobre tipos de jefes: «el que micromanagea», «el compa inapropiado», y así. Reconocí al nuestro como una mezcla de «el tirano» y «el incompetente». Un poco maquiavélico, pero no lo suficientemente inteligente para serlo por completo. Si tenías una idea que querías implementar tenías que convencerlo de que era su idea. Escribíamos para unos cuantos productos: Reddish Fish, una imitación de Swedish Fish. Un tipo especial de media atlética antibacterial. Recordaba las empresas y no podía recordar la cara de mi madre ni de mi padre.

Al inicio aquí veía a un hombre y me decía a mi misma, su barbilla se parece a la de Adam, o sus manos, con sus grandes uñas rectangulares, se parecen a las de Adam. Pero después de una década ya no podía recordar esas manos, ni su pecho, ni incluso su tamaño. ¿Era alto? ¿Era bajo? Me aferré tan fuerte como pude al recuerdo de su cara, la cara de Adam. Cada mañana lo reconstruía a partir de los hechos a los que me aferraba. El color de sus ojos: avellana. El color de su pelo: café oscuro. El largo de sus pestañas: largas. Su cuerpo se fue y su olor se fue, pero su cara la tenía, y mantenía, juré mantenerla por siempre.

Tal vez era una fijación enferma, pero él fue el amor, sabés, de mi vida. Desde una perspectiva meramente estadística, él fue el setenta y cinco por ciento de mis relaciones.

Todes en el cielo tenían treinta y tres años, como Jesús. Yo envejecí tres años, y les bebés envejecen un montón de años. Les hombres y mujeres viejes se despojan de ellos, pero retienen su coeficiente intelectual emocional y sus años de experiencia vividos.

Se lo que te estás preguntando, y la respuesta es sí: en el cielo las personas están buenísimas. Treinta y tres es una edad ardiente, especialmente cuando tenés la sabiduría de una persona de ochenta y cinco. Y como podés esperar, octogenaries en cuerpos de treinta y tres años son un poco... bueno, digamos que tienen una cosa en sus cabezas.

Por supuesto, el sexo no era por lo que estaba aquí. O sea, no que estuviera aquí por alguna razón en particular. Solo iba a estar aquí, y tenía que sacar lo mejor de la situación. No tener que trabajar en la agencia era lindo. Tomaré nuevos pasatiempos, pensé. Me uní a un club de libros. Tomé clases de dibujo, más que nada para poder dibujar caras, incluyendo la de ya saben quien. Después de veinte años mis habilidades de raquetbol dieron un giro y empecé a ganarle a Heidi, lo que nos sorprendió a ambas. Tenía, por primera vez en mi «vida», un físico en forma, atlético. No que mi corazón pudiese colapsar, no que la salud cardiovascular importase. Pero mi nuevo cuerpo se veía muy bien, por un rato igual, hasta que ya no fue una novedad.

Día tras día trataba de recordar hechos de mi anterior esposo, los ojos dorados y el pelo café y las pestañas largas. Dibujé su cara una y otra vez en mi clase, aunque nunca se veía completamente correcto. ¿Y cómo se llamaba? A veces no podía recordar y tenía que revisar. ADAM, escribí en pequeñas, casi imperceptibles letras en el muro de mi cuarto. Pero el conserje seguía borrándolo. Entonces lo escribí, ADAM, en un pedazo de papel, y lo escondí debajo de mi sábana, y recordaba sacarlo antes de cada día de lavandería, que eran los martes.

Tuvimos nuestros problemas, Adam y yo. Por supuesto que sí. Pero nos respetábamos, y eso para mi siempre fue lo principal. Hablábamos a pesar de nuestras diferencias. Las conversaciones siempre ayudaron, pero en mi mente, reforzaron lo separades que eramos como personas. El día que nos casamos vi en sus ojos un destello de preocupación por lo que estábamos a punto de embarcar. Ese destello fue como un pedazo de vidrio que voló sobre mi y me atrapó como unas pantimedias a un pedazo de piel seca, como una rebaba a una media, y tuve que disculparme mientras iba de nuestra mesa a llorar en el caro y alfombrado baño portátil. «Lágrimas de júbilo», dije cuando regresé y unas lágrimas se me salieron. Y Adam sonrió, y succionó las gotas de mi cara con su boca, una broma que teníamos.

Por alguna razón, revisaba constantemente el papel debajo de mi colchón para recordarme el nombre de mi esposo. Era vergonzoso lo frecuente que lo hacía. Trataba de distraerme con otras actividades: jugaba raquetbol con Heidi, y a veces comíamos brunch en los fines de semana. Horneaba pasteles para amigues, experimentando con diferentes recetas y harinas alternativas. No me detenía mucho tiempo a preguntarme las razones porque lo probé una vez, y el resultado fue que pase un año sólo llorando, lo que dejó mi cara completamente destrozada, lo que tomó otro año para que se deshinchara.

¿Qué habíamos hecho mi esposo y yo? A veces anotaba cosas, como una lista de compras: Yo arrancaba su carro cuando él dejaba las luces encendidas. Él me mordía la parte de atrás de las rodillas, de forma juguetona. Veíamos televisión por streaming. Teníamos cenas con mis padres inmigrantes y les suyes normales. Nos tomábamos selfies con nuestros teléfonos desde ángulos locos y nos las enviábamos, tratando de sobrepasar lo horrible de le otre. Antes de casarnos hablábamos con rodeos del matrimonio. ¿Qué sería? ¿qué significaría para las personas? ¿era algo que íbamos a hacer? A veces las charlas terminaban conmigo yéndome, para que mi boca se pudiera contorsionar de una forma ridícula vuelta hacia abajo, y pudiera llorar en silencio. Nunca había una advertencia, la tristeza me superaba de repente. Cuando respondíamos a algo simple como «¿qué tal tu día?», o «¿cómo estuvo el trabajo», yo escuchaba y esperaba una respuesta más sincera: cómo se sentía y a qué le tenía miedo, una revelación de qué le decía una región de su cerebro a otra de las regiones de su cerebro. Apuesto que era agotador para él. Era agotador para mi también.

Al final fue mi esposo por un día. ¿Yo era demasiado exigente? Sólo quería conocerlo. Pensé que ese era el punto: conocer gente. ¿Pero qué sabía, realmente?

Un martes estaba ocupada destruyendo a Heidi en raquetbol, y olvidé esconder del ama de casa el pedazo de papel que decía ADAM, y cuando regresé mis sábanas estaban recién lavadas y el pedazo de papel ya no estaba. «No llorés», me ordené a mi misma, en voz alta, sintiendo que estaba a punto de hacerlo. Llamé a Heidi y nos emborrachamos con ginebra artesanal y no lloré aunque no podía creérmelo, no podía creer lo que había hecho. Puta estúpida, me repetía a mi misma esa noche, increíblemente borracha. ¿Cómo pudiste? Y Heidi repetía lugares comunes como budistas, como, Cultivá el mindfulness. Ella había sido madre, y había tenido un ataque al corazón a la edad de cincuenta y seis. Ella ya no podía recordar cuántes hijes había tenido, mucho menos ningún hecho sobre elles.

«¿De dónde sos?», me preguntaba la gente a veces, admirando mi cara.

«California», decía.

«O sea, ¿de dónde sos originalmente?», preguntaban. Y yo pensaba, Vaya. Pensaba, ¿Esto en realidad está pasando aquí?

Veía a Richard a veces en el supermercado. Teníamos una conversación liviana. Resulta que no era mi carcelero, sólo estaba haciendo trabajo comunitario, de voluntario con las personas nuevas.

De alguna forma las cosas empezaron a cambiar. Sin siquiera intentarlo conocí a un tipo. Fue en el estudio de arte, donde estaba dibujando lo que podía recordar de la cara de mi esposo, que recientemente no era mucho. Sólo que tenía una, con cosas regulares en ella. Y este tipo estaba haciendo una taza. «¿Quién es ese?», preguntó. Y aunque el nombre de mi esposo estaba en la punta de mi lengua, no lo sabía. Me eludió. «Lo siento», dijo, cuando se dio cuenta de lo preocupada que parecía por no saber la respuesta.

«Yo soy Adam», dijo.

«Hola Adam», dije, gustándome el nombre, gustándome cómo sonaba cuando lo dije. Adam me preguntó si quería ir a cenar alguna vez. «No lo se...», empecé. «Soy nuevo aquí», dijo muy amable. Yo casi digo, «Yo también soy nueva aquí», pero en realidad ya había estado aquí más de cincuenta años.

Pedimos hamburguesas desde el carro. Hicimos plática casual. Él me dijo que había muerto de forma pacífica mientras dormía. Tenía ochenta y cuatro. Era navidad. Él tenía neumonía y su esposa estaba dormida en el cuarto, lo había estado cuidando. Tuvo un lindo día con sus hijas e hijo, y sus familias. Dijo con un poco de nostalgia que la suya fue una muy buena.

«¿Muerte?», pregunté.

«Vida quise decir», dijo. «Pero sí, eso también.»

Me preguntó algunas preguntas básicas de ser nuevo en la muerte, como: ¿Puedo comerme lo que sea que me de la gana sin ganar peso? Y: ¿Te lavan? (Respuestas: sí y sí). Ordenó un batido de chocolate y unas papas con chili y queso, y se limpió las manos grasosas en sus pantalones.

 «¿Podés ver a tu familia?», preguntó. «¿Como un fantasma?»

Y yo le dije, «No, por su puesto que no». Un poco insensible, me di cuenta, cuando lo vi desinflado. «Lo siento», dije.

«Tiene sentido», dijo.

Era fácil hablar con Adam. Hacía muchas bromas, algunas me hacían quejarme y unas pocas me hicieron reír y reír como no lo había hecho en años. No me preguntó de dónde era originalmente, ni intentó bajarme los pantalones. Me cuesta sentirme cómoda con personas nuevas, en general, pero de alguna forma él me hizo sentir a gusto. Es deprimente que este tipo de personas aparece sólo una vez cada medio siglo, pero de nuevo, trato de no obsesionarme.

«Me recordás a alguien», dijo. La palabra «recordar» ya era prácticamente extraña para mi. Sabía que significaba algo para algunas personas, y podía entenderla en teoría, pero no creía haberla sentido alguna vez.

«¿Podemos hacer esto de nuevo alguna vez?», me preguntó.

«Pues», le dije, «es que estoy como ocupada estos días».

«Es broma», agregué rápidamente, cuando lo vi decepcionado de nuevo. «¡Fue una broma! Pronto la entenderás», dije.

«¿Mañana?» dijo él.

«Claro, mañana», dije.

Él se volvió para irse, y luego se detuvo.

«¿De dónde me dijiste que sos?», preguntó.

Me guardé un suspiro.

«California», dije.

«Pero, ¿como en dónde en California?» preguntó.

«No recuerdo», dije, honestamente.

«Ah», dijo. «Bueno, no importa. Nos vemos mañana».

La siguiente noche quedamos en encontrarnos en la torre del reloj. En la plaza, doce mujeres chinas que me recordaron a mi madre estaban haciendo ejercicios sincronizados. Todas tenían mi edad, aunque estoy segura que tenían cien cuando murieron. Todas las noches son luna llena, pero la luna cambia de color. Hoy era color lavanda. Bajo la luna lavanda las señoras brillaban morado, moviendo sus brazos, cantando en chino, y Adam rodeándolas, buscándome, parecía nervioso. «¡Ey!» le grité. De inmediato su cara cambió aliviada.

Tenía una bolsa de papel con dos sánguches submarinos. Estaban cortados a la mitad, entonces intercambiamos mitades y nos los comimos en una banca cerca del río. «Extraño a mi esposa», dijo suavemente. «Extraño a mis hijes».

«Es difícil», dije, aunque no lo recordaba. ¿Fue difícil?

«Es difícil», dijo él de acuerdo.

Se puso de pie para limpiarse las migajas y empezamos a pasear por el río. Él no había terminado su sánguche, entonces agarró la bolsa de papel mientras caminamos. Deseé tener algo significativo que decirle, a cambio por su vulnerabilidad. Más temprano vencí a Heidi en raquetbol. Boté una vasija. Mi plan era mantener flores frescas en la casa con más frecuencia. Levanté mi colchón y revisé abajo, por alguna razón. No sé por qué.

Quería preguntarle más acerca de su esposa e hijes, pero nos desalentaban a alentar la nostalgia de les nueves residentes. ¿De qué más podíamos hablar que nos hiciera mejores amigues y nos acercara? Atormenté mi cerebro mientras deambulábamos.

Un perro se nos acercó. Un hermoso perro dorado que tenía treinta y tres en años de perro, como todos los perros aquí. No tenía dueñe, ni collar. Dejó que Adam acariciara su cabeza dorada, así que yo me acerqué para tocarlo. Él retrocedió. Me vio, empezó a gruñir y después a ladrar. Estaba ladrándome a mi, específicamente. Esto era exasperante pero familiar: este perro, desafortunadamente, era racista.

«¿Hay perros racistas aquí?», dijo Adam, escéptico.

Me encogí de hombros, decepcionada pero resignada. «Hay perros racistas en todos lados».

Adam metió la mano en su bolsa de papel y cortó un poco de carne de su sánguche. Le dio al perro racista un poco de carne. El perro racista cambió su tonada. Ahora el perro racista nos estaba rogando por más.

«Ey», Adam me alcanzó la bolsa del sánguche y yo también corté un poco de carne. Me arrodillé y se la di al perro.

«Aquí, estúpido», susurré.

Alimentamos al perro con el resto del sánguche de Adam, y seguimos nuestro camino. Las mujeres danzando ya no estaban danzando, sino platicando. El perro racista nos siguió. Ocasionalmente nos volvíamos para ver si aún estaba ahí. Ahí estaba.

«Tal vez deberíamos dejárnoslo», dijo Adam.

«¿Dejarnos ese perro racista?»

«Ya se reformó», dijo Adam. «Él podría recompensártelo».

Esa era la fortaleza de mi esposo, recordé de repente. Era bueno calmándote y al mismo tiempo no estabas cien por ciento segura de que realmente entendiera.

Entonces tomé el brazo de Adam. Era una bella noche, el morado reflejándose en los árboles, en el agua, en las casas. Me sentí un poco enamorada, y luego un montón.

«Tenés lindas orejas», dijo, acomodando mi pelo detrás de ellas, ya olvidando a su esposa e hijes.

Al final nos dejamos al perro. Resultó ser una perra, y la  llamamos Betsy, y le enseñamos a no ser racista, sexista, ni intolerante de ninguna forma. Adam y yo, terminamos estando juntes por cien años, muchos de ellos geniales. Llevábamos a Betsy a largos paseos a pie, jugamos tenis en pareja, dejamos nuestras ropas en la casa de le otre, hablamos de nuestros miedos. Fuimos tolerantes, esto fue amor. Y aunque la separación fue amistosa, yo lloré y lloré de todas formas. Y él succionó cada una de las lágrimas de mi cara como una aspiradora.

«Nunca te he visto llorar», me quejé. «Ni una vez en cien años».

Y Adam vio a la distancia, pareció concentrarse mucho, y exprimió una única lágrima de la esquina de su ojo.

«Traé una cuchara», dijo.

Deslizamos la lágrima en la cuchara. Luego me besó, gentilmente, en la mejilla, y se fue.

Yo usé la cuchara con la lágrima para mezclar el azúcar en mi café, que es delicioso aquí. El más justo de los tratos. Me tomé mi tiempo tomando el café. Era celestial: suave y nada amargo. Sabía a flores y tierra. Cuando terminé, puse la taza en el fregadero, y aunque teníamos conserjes, por alguna razón lavé la taza y la cuchara. Ya que estaba en eso, me lavé la cara. Me puse los zapatos de correr y los audífonos en mis oídos. Toqué mis tobillos y estiré. Salí corriendo de la casa escuchando música pop. Afuera era un día soleado.

Esto es lo que sé: Alguien de mi pasado me importaba un montón. Tuvimos una relación linda e irreemplazable que era una en un millón. A veces le escribiré una carta a él o ella. «Mi queride vos», así empezará.
