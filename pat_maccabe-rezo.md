# Rezo dirigido por Pat Maccabe, Mujer de Pie Brillando

Juntes, hacemos un lugar en el que la vida floreciente, el Futuro y lo que es Sagrado se pueden encontrar, alinear, armonizar, y tener acceso a este constructo de nuestra imaginación que llamamos «dinero».

Donde todo lo que ha sido extraído de la Madre Tierra, de los cuerpos de hombres, mujeres, y niñes, del cuerpo de los océanos, del cielo, de los cuerpos de les Voladores, les Nadadores, les Bichitos Rastreros, les de 4 patas, de las plantas, de la nación que está de pie, de los árboles, de las piedras, del suelo, de las aguas, de los órganos internos de la Madre Tierra: carbón, oro, petróleo, etc.

De la bondad de les de cinco dedos, de nuestro amor, nuestras psiques, y de nuestras almas.

De cada lugar desde el que ha sido extraído y reunido, de todos los lugares donde el flujo de lo que es necesario para una vida cíclica, en espiral, ha sido confinado, dirigido, canalizado, en formas que no sirven la vida floreciente, o el amor, o a la luz, que no sirven futuro.

Todo este diezmado de la Belleza, Armonía, Balance, Sustancia y del Sagrado Movimiento de Vida, es ahora, aquí, reconocido, reconciliado, venerado, y puesto en las manos sanadoras y la Voluntad de una profunda y Sagrada Cocreación. Ahora es liberada, fluyendo y fluyendo y fluyendo, una represa liberada, a los lugares más críticos, a los lugares más generativos, a los lugares más restauradores, a los lugares más alegres, a los lugares más creativos para la Totalidad e Integridad de Interser, a los lugares más sagrados, a los lugares más afirmativos de nuestro verdadero deseo y nuestro más profundo Consentimiento hacia la Vida, hasta Siete Generaciones, para vivir con todas nuestras relaciones en esta Tierra Sagrada, Floreciente, Poderosa, para alinear su ilimitada creatividad, Ingenuidad y Amor incondicional en la Creación de una Vida sin Fin, Siempre en Evolución.

Damos nuestro amor, aquí.

Damos nuestro lugar en Generosidad sin Miedo, aquí.

Damos nuestra Fé, aquí.

Damos nuestro lugar en Abundancia Radical, aquí.

Damos nuestro consentimiento, aquí.

Con esta apertura, rompemos la esclavitud que era Ley por encima del Amor.

Abrimos y nos preparamos para amar el futuro, en formas que el paradigma de la escasez no permitiría. Le damos poder a este tercer cajón, y le pedimos que nos de Futuro. Con nuestra participación en esta Reunión, con el Regalo Sagrado de nuestra Voluntad, desde el lugar en el Arco de Vida Sagrado que nos ha sido dado para sostener. Abrimos este campo del Tercer Cajón, para recibir el Futuro de la más alta posibilidad para la Vida, Luz, y Amor.

Que todo lo que este flujo encuentre sea tocado y transformado, en cualquier forma que sea correcta, por esta moneda de Conciencia que nos ha alcanzado, y que fluya adentro y a través de la Conciencia Futura, hacia la que estamos creciendo.

Vida Santa, Futuro Santo, por favor, guíennos y diríjannos.

Muestrennos la(s) Forma(s).

