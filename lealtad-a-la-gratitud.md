# Lealtad a la gratitud

> Versión de John Stokes y Kanawahientun de 1993, publicada en Braiding Sweetgrass de Robin Wall Kimmerer.

Hoy nos hemos reunido y vemos las caras que nos rodean y vemos que los ciclos de la vida continúan. Nos han dado el deber de vivir en balance y armonía unes con otres y con todas las cosas vivientes. Entonces ahora unamos nuestras mentes como una mientras damos saludos y gracias le une a le otre como Pueblo. Ahora nuestras mentes son una.

Nosotres estamos agradecides con nuestra Madre la Tierra, ya que nos da todo lo que necesitamos para la vida. Da soporte a nuestros pies mientras caminamos sobre ella. Nos da alegría que ella todavía sigue cuidando de nosotres, tal como lo ha hecho desde el inicio del tiempo. A nuestra Madre, le enviamos acción de gracias, amor y respeto. Ahora nuestras mentes son una.

Damos gracias a todas las aguas del mundo por saciar nuestra sed, por brindar fuerza y nutrir vida para todes les seres. Sabemos de su poder en muchas formas: cascadas y lluvia, brumas y arroyos, ríos y océanos, nieve y hielo. Estamos agradecides de que las aguas aún estén aquí y cumpliendo su responsabilidad para el resto de la Creación. ¿Podemos estar de acuerdo con que el agua es importante para nuestras vidas y unir nuestras mentes como una para enviar saludos y gracias al Agua? Ahora nuestras mentes son una.

Giramos nuestros pensamientos a toda la vida de les Peces en el agua. Elles fueron instruides para limpiar y purificar el agua. También se entregan a nosotres como comida. Estamos agradecides de que continúen con sus deberes y enviamos a les peces nuestros saludos y nuestras gracias. Ahora nuestras mentes son una.

Ahora nos giramos hacia los vastos campos de la vida de las Plantas. Tan lejos como el ojo puede ver, las Plantas crecen, haciendo muchas maravillas. Ellas sostienen muchas formas de vida. Con nuestras mentes unidas, damos gracias y esperamos ver vida vegetal por muchas generaciones por venir. Ahora nuestras mentes son una.

Cuando miramos a nuestro alrededor, vemos que las bayas aún están aquí, brindándonos comidas deliciosas. La líder de las bayas es la fresa, la primera en madurar en la primavera. ¿Podemos estar de acuerdo en que estamos agradecides de que las bayas estén con nosotres en el mundo y enviar nuestra acción de gracias, amor y respeto a las bayas? Ahora nuestras mentes son una.

Con una sola mente, honramos y agradecemos a todas las Plantas Alimenticias que cosechamos del jardín, en especial a las Tres Hermanas que alimentan a las personas con tanta abundancia. Desde el inicio del tiempo, los granos, vegetales, frijoles y frutas han ayudado a las personas a sobrevivir. Muchas otras cosas vivas toman fuerza de ellas también. Unimos en nuestras mentes a todas las plantas alimenticias y les enviamos un saludo y agradecimiento. Ahora nuestras mentes son una.

Ahora nos giramos a las Hierbas Medicinales del mundo. Desde el inicio se les instruyó para llevarse la enfermedad. Ellas siempre están esperando y listas para sanarnos. Estamos tan felices de que aún haya entre nosotres algunas de aquellas especiales que recuerdan cómo usar las plantas para sanar. Con una sola mente, enviamos acción de gracias, amor y respeto a las Medicinas y a las guardianas de las Medicinas. Ahora nuestras mentes son una.

De pie a nuestro alrededor están les Árboles. La Tierra tiene muchas familias de Árboles, cada una con sus propias instrucciones y usos. Algunas brindan refugio y sombra, otras frutas y belleza y muchos otros regalos útiles. El Maple es el líder de los árboles, para reconocer su regalo de azúcar cuando las Personas lo necesitan más. Muchas personas del mundo reconocen al Árbol como un símbolo de paz y fuerza. Con una sola mente saludamos y agradecemos a la vida de les Árboles. Ahora nuestras mentes son una.

Unimos nuestras mentes para enviar nuestros saludos y gracias a toda la hermosa vida animal del mundo, que se pasea con nosotres. Elles tienen muchas cosas que enseñarnos a las personas. Estamos agradecides de que continúen compartiendo sus vidas con nosotres y esperamos que siempre sea así. Juntemos nuestras mentes como una y enviemos nuestras gracias a les Animales. Ahora nuestras mentes son una.

Ponemos nuestras mentes juntas como una y agradecemos a todes les pájares que se mueven y vuelan sobre nuestras cabezas. La Creadora les dio el regalo de las canciones hermosas. Cada mañana saludan el día y con sus canciones nos recuerdan disfrutar y apreciar la vida. El Águila fue escogida para ser su líder y para vigilar el mundo. A todes les Pájares, desde le más pequeñe a le más grande, les enviamos nuestros alegres saludos y gracias. Ahora nuestras mentes son una.

Estamos agradecides con los poderes que conocemos como los Cuatro Vientos. Oímos sus voces en el aire en movimiento mientras nos refrescan y purifican el aire que respiramos. Elles ayudan a traer el cambio de las estaciones. Desde las cuatro direcciones vienen, trayéndonos mensajes y dándonos fuerza. Con una sola mente enviamos nuestros saludos y gracias a los Cuatro Vientos. Ahora nuestras mentes son una.

Ahora nos giramos al oeste donde viven nuestres abueles, les Seres del Trueno. Con voces relampagueantes y tronantes traen con elles el agua que renueva la vida. Juntamos nuestras mentes como una y enviamos nuestros saludos y gracias a nuestres Abueles, les Tronadores.

Ahora enviamos nuestros saludos y gracias a nuestro hermano mayor el Sol. Cada día sin fallo él viaja por el cielo de este a oeste, trayendo la luz de un nuevo día. Él es nuestra fuente de todos los fuegos de vida. Con una mente, enviamos saludos y gracias a nuestro Hermano, el Sol. Ahora nuestras mentes son una.

Unimos nuestras mentes y damos gracias a nuestra más vieja Abuela, la Luna, que ilumina el cielo de noche. Ella es la líder de todas las mujeres de todo el mundo y gobierna los movimientos de las mareas oceánicas. Por el cambio de su cara medimos el tiempo y es la Luna quien vigila la llegada de niñes aquí en la Tierra. Juntemos nuestros agradecimientos para la Abuela Luna en una pila, capa sobre capa de gratitud, y luego alegremente lancemos esa pila de gracias alto hacia el cielo de noche para que ella sepa. Con una sola mente, enviamos saludos y gracias a nuestra Abuela, la Luna.

Damos gracias a las Estrellas, que están esparcidas a lo largo del cielo como joyas. Las vemos de noche, ayudando a la Luna a iluminar la oscuridad y trayendo rocío a los jardines y haciendo crecer cosas. Cuando viajamos de noche, ellas nos guían hacia el hogar. Con nuestras mentes unidas como una, enviamos saludos y gracias a todas las Estrellas. Ahora nuestras mentes son una.

Unimos nuestras mentes para saludar y agradecer a les Maestres iluminades que han venido a ayudarnos a lo largo de los siglos. Cuando olvidamos cómo vivir en armonía, elles nos recuerdan la forma que fuimos instruides para vivir como pueblo. Con una mente, enviamos saludos y gracias a estes cuidadoses Maestres. Ahora nuestras mentes son una.

Ahora giramos nuestros pensamientos a la Creadora, o Gran Espíritu, y enviamos saludos y gracias por todos los regalos de la Creación. Todo lo que necesitamos para vivir una buena vida está aquí en la Madre Tierra. Por todo el amor que aún está entre nosotres, unimos nuestras mentes como una y enviamos nuestras palabras más selectas de saludos y gracias a la Creadora. Ahora nuestras mentes son una.

Ahora hemos llegado al lugar en que terminamos nuestras palabras. De todas las cosas que hemos nombrado, no es nuestra intención dejar nada fuera. Si algo fue olvidado, dejamos que cada individue envíe esos saludos y gracias en su propia forma. Ahora nuestras mentes son una.
